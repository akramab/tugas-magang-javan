public class CetakGanjilGenap {
    public static void cetakGanjilGenap(int start, int end) {
        for (int i = start; i <= end; i++) {
            if (i%2 == 0) {
                System.out.println("Angka "+i+" adalah genap");
            }else {
                System.out.println("Angka "+i+" adalah ganjil");
            }
        }
    }
}