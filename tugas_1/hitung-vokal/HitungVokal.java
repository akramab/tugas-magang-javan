import java.util.Arrays;

public class HitungVokal {
    private static Character[] vocals = {'a','i','u','e','o'};

    public static void hitung(String input) {
        Character[] result = new Character[5];

        int idx = 0;
        int count = 0;
        for(char c : input.toCharArray()) {
            if (Arrays.asList(vocals).contains(c)) {
                if (!Arrays.asList(result).contains(c)) {
                    result[count] = c;
                    count++;
                }
            }
            idx++;
        }

        if (count > 0) {
            for (int i = 0; i < count; i++) {
                if (i == 0) {
                    System.out.print('"'+input+"\" = "+count+" yaitu "+ result[i]);
                } else if (i == (count-1)){
                    System.out.print(" dan "+result[i]+"\n");
                } else {
                    System.out.print(" dan "+result[i]);
                }
            }
        } else {
            System.out.println('"'+input+"\" tidak memiliki huruf vokal!");
        }
    }
}