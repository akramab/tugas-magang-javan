public class Kalkulator {
    public static float hitung(String input) throws ArithmeticException {
        //Asumsikan input selalu terdiri atas 2 operand dan 1 operator
        String[] parsedInput = input.split(" ");

        Integer num1 = Integer.valueOf(parsedInput[0]);
        Integer num2 = Integer.valueOf(parsedInput[2]);
        Character operand = Character.toLowerCase(parsedInput[1].charAt(0));

        switch(operand) {
            case 'x':
                return num1 * num2;
            case '/':
                if(num2 == 0) {
                    throw new ArithmeticException("tidak bisa membagi dengan 0!");
                }
                return num1/num2;
            case '+':
                return num1+num2;
            case '-':
                return num1-num2;
            default:
                return -1;
        }
    }
}