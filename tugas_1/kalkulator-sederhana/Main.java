public class Main {
    public static void main(String[] args) {
        try {
            System.out.println(Kalkulator.hitung("2 + 2"));
            System.out.println(Kalkulator.hitung("2 x 2"));
            System.out.println(Kalkulator.hitung("2 / 2"));
            System.out.println(Kalkulator.hitung("2 / 0"));
        } catch (ArithmeticException e) {
            System.out.println(e.getMessage());
        }
    }
}