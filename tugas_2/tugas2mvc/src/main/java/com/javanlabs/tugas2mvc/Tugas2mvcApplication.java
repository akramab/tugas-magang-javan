package com.javanlabs.tugas2mvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tugas2mvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(Tugas2mvcApplication.class, args);
	}

}
