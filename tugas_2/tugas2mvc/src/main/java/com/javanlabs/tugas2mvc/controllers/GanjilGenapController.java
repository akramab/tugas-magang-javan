package com.javanlabs.tugas2mvc.controllers;

import com.javanlabs.tugas2mvc.domain.GanjilGenap;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class GanjilGenapController {

    @GetMapping("/ganjilgenap")
    public String showGanjilGenap(@ModelAttribute("ganjilGenap") GanjilGenap ganjilGenap) {
        return "ganjil_genap_input";
    }

    @PostMapping("/ganjilgenap")
    public String showHasil(@ModelAttribute("ganjilGenap") GanjilGenap ganjilGenap) {

        return "ganjil_genap_input";
    }
}
