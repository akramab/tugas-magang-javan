package com.javanlabs.tugas2mvc.controllers;

import com.javanlabs.tugas2mvc.domain.GanjilGenap;
import com.javanlabs.tugas2mvc.domain.HurufVokal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class HurufVokalController {

    @GetMapping("/hurufvokal")
    public String showGanjilGenap(@ModelAttribute("hurufVokal") HurufVokal hurufVokal) {

        return "huruf_vokal_input";
    }

    @PostMapping("/hurufvokal")
    public String showHasil(@ModelAttribute("hurufVokal") HurufVokal hurufVokal) {

        return "huruf_vokal_input";
    }
}
