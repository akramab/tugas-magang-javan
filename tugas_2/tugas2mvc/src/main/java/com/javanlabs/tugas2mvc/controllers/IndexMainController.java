package com.javanlabs.tugas2mvc.controllers;

import com.javanlabs.tugas2mvc.domain.GanjilGenap;
import com.javanlabs.tugas2mvc.domain.HurufVokal;
import com.javanlabs.tugas2mvc.domain.Kalkulator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class IndexMainController {

    @GetMapping("/")
    public String showGanjilGenap(Model model) {
        GanjilGenap ganjilGenap = new GanjilGenap();
        HurufVokal hurufVokal = new HurufVokal();
        Kalkulator kalkulator = new Kalkulator();

        model.addAttribute("ganjilGenap", ganjilGenap);
        model.addAttribute("hurufVokal", hurufVokal);
        model.addAttribute("kalkulator", kalkulator);

        System.out.println("Success");

        return "index";
    }
}
