package com.javanlabs.tugas2mvc.controllers;

import com.javanlabs.tugas2mvc.domain.Kalkulator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class KalkulatorController {

    @GetMapping("/kalkulator")
    public String showKalkulator(@ModelAttribute("kalkulator") Kalkulator kalkulator) {

        return "kalkulator_input";
    }

    @PostMapping("/kalkulator")
    public String showHasil(@ModelAttribute("kalkulator") Kalkulator kalkulator) {
        return "kalkulator_input";
    }
}
