package com.javanlabs.tugas2mvc.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GanjilGenap {
    private Integer start;
    private Integer end;

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getEnd() {
        return end;
    }

    public void setEnd(Integer end) {
        this.end = end;
    }

    public List<List<String>> hitung() {
        List<List<String>> hasil = new ArrayList<>();

        for (int i = start; i <= end; i++) {
            if (i%2 == 0) {
                hasil.add(Arrays.asList(String.valueOf(i), "genap"));
            } else {
                hasil.add(Arrays.asList(String.valueOf(i), "ganjil"));
            }
        }

        return hasil;
    }
}
