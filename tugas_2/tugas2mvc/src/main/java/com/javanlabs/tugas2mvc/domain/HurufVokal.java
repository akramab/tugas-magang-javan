package com.javanlabs.tugas2mvc.domain;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.util.Arrays;
import org.owasp.esapi.ESAPI;


public class HurufVokal {
    private String text;
    private static Character[] vocals = {'a','i','u','e','o'};

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String hitung() {
        Character[] result = new Character[5];

        text = stripXSS(text);
        System.out.println(text);

        int idx = 0;
        int count = 0;
        for(char c : text.toCharArray()) {
            if (Arrays.asList(vocals).contains(Character.toLowerCase(c))) {
                if (!Arrays.asList(result).contains(Character.toLowerCase(c))) {
                    result[count] = c;
                    count++;
                }
            }
            idx++;
        }

        if (count > 0) {
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < count; i++) {
                if (i == 0) {
                    str.append('"'+text+"\" = "+count+" yaitu "+ result[i]);
                } else {
                    str.append(" dan "+result[i]);
                }
            }
            return str.toString();
        } else {
            return ('"'+text+"\" tidak memiliki huruf vokal!");
        }
    }

    public static String stripXSS(String value) {
        if (value == null) {
            return null;
        }
        value = ESAPI.encoder()
                .canonicalize(value)
                .replaceAll("\0", "");
        return Jsoup.clean(value, Whitelist.none());
    }

    @Override
    public String toString() {
        return "HurufVokal{" +
                "text='" + text + '\'' +
                '}';
    }
}
