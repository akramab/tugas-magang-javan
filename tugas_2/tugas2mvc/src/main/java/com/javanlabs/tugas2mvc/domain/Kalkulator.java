package com.javanlabs.tugas2mvc.domain;

public class Kalkulator {
    private String input;
    private char operator;

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public char getOperator() {
        return operator;
    }

    public void setOperator(char operator) {
        this.operator = operator;
    }

    public String hitung() {
        String[] parsedInput = input.split(" ");

        Double num1 = Double.valueOf(parsedInput[0]);
        Double num2 = Double.valueOf(parsedInput[2]);
        Character operand = Character.toLowerCase(parsedInput[1].charAt(0));

        switch(operand) {
            case '*':
                return String.valueOf((num1 * num2));
            case '/':
                if(num2 == 0) {
                    return "tidak bisa membagi dengan 0!";
                }
                return String.valueOf(num1/num2);
            case '+':
                return String.valueOf(num1+num2);
            case '-':
                return String.valueOf(num1-num2);
            default:
                return "Error";
        }
    }
}
