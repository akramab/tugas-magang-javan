-- Mencari CEO
SELECT nama as nama_CEO FROM employee
WHERE atasan_id IS NULL;

-- Mencari Staff Biasa
SELECT nama as nama_staff_biasa from employee
WHERE id NOT IN (SELECT DISTINCT atasan_id FROM employee WHERE atasan_id IS NOT NULL);

-- Mencari Direktur
SELECT nama as nama_direktur from employee
WHERE atasan_id = (SELECT id FROM employee WHERE atasan_id IS NULL);

-- Mencari Manager
SELECT nama as nama_manager from employee
WHERE atasan_id IN  (SELECT DISTINCT id from employee
                    WHERE atasan_id = (SELECT id FROM employee WHERE atasan_id IS NULL));

-- Query dengan parameter (menggunakan procedure)
DROP PROCEDURE IF EXISTS jumlahBawahan;
DELIMITER //

CREATE PROCEDURE jumlahBawahan(IN nama_employee VARCHAR(50))
BEGIN



WITH RECURSIVE
bawahan
  (parent_id, bawahan, lvl)
AS
  (SELECT atasan_id, id, 1
    FROM employee
  UNION ALL
    SELECT d.parent_id, s.id, d.lvl + 1
    FROM bawahan  d
    JOIN employee  s
    ON d.bawahan = s.atasan_id
  )
SELECT k.nama, COUNT(hasil.bawahan) AS jumlah_bawahan
FROM bawahan as hasil, employee AS k
WHERE hasil.parent_id IS NOT NULL AND k.id = hasil.parent_id AND k.nama = nama_employee
GROUP BY hasil.parent_id;

END //
DELIMITER;

-- Masukkan nama employee yang ingin dicari jumlah bawahannya
CALL jumlahBawahan('Pak Budi');



