-- Idenya adalah mendapatkan data karyawan yang id nya tidak ada di kolom atasan_id
-- karena status di bawah manajer, artinya ia tidak memiliki bawahan.

SELECT nama as nama_pegawai FROM karyawan
WHERE id NOT IN (SELECT DISTINCT atasan_id FROM karyawan WHERE atasan_id IS NOT NULL);