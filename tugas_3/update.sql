-- Menggunakan operator LIKE karena informasi yang diberikan dari soal hanya sebagian namanya saja
-- Asumsi sebagian nama yang diberikan adalah unik pada table karyawan

UPDATE karyawan
SET status = 'Menikah'
WHERE nama LIKE '%Farhan%' OR nama LIKE '%Nadia%' OR nama LIKE '%Abi%' OR nama LIKE '%Dani%';