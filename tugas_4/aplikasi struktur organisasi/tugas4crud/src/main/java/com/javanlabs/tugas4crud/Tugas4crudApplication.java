package com.javanlabs.tugas4crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tugas4crudApplication {

	public static void main(String[] args) {
		SpringApplication.run(Tugas4crudApplication.class, args);
	}

}
