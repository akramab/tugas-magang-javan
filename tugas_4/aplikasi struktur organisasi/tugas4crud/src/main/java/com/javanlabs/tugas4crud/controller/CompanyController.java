package com.javanlabs.tugas4crud.controller;

import com.javanlabs.tugas4crud.entity.Company;
import com.javanlabs.tugas4crud.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CompanyController {

    @Autowired
    private CompanyService service;

    //For web pages
    @GetMapping("/companies")
    public String viewCompanies(Model model) {
        model.addAttribute("listOfCompanies", service.getCompanies());

        return "companies_list";
    }

    @GetMapping("/new-company-form")
    public String viewNewCompanyForm(Model model) {

        Company company = new Company();
        model.addAttribute("company", company);
        return "new_company_form";
    }

    @PostMapping("/addCompany")
    public String addCompany(@ModelAttribute("company") Company company)  {
        service.saveCompany(company);

        return("redirect:/companies");
    }

    @GetMapping("/update-company/{id}")
    public String viewUpdateCompany(@PathVariable (value = "id") Integer id, Model model) {
        Company company = service.getCompanyById(id);

        model.addAttribute("company", company);
        return "update_company_form";
    }

    @GetMapping("/delete-company/{id}")
    public String deleteCompany(@PathVariable (value = "id") Integer id) {
        service.deleteCompany(id);
        return("redirect:/companies");
    }
}
