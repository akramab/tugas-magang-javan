package com.javanlabs.tugas4crud.controller;

import com.javanlabs.tugas4crud.entity.Employee;
import com.javanlabs.tugas4crud.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class EmployeeController {

    @Autowired
    private EmployeeService service;

    //For web pages
    @GetMapping("/employees")
    public String viewEmployees(Model model) {
        model.addAttribute("listOfEmployees", service.getEmployees());

        return "employees_list";
    }

    @GetMapping("/new-employee-form")
    public String viewNewEmployeeForm(Model model) {

        Employee employee = new Employee();
        model.addAttribute("employee", employee);
        return "new_employee_form";
    }

    @PostMapping("/addEmployee")
    public String addEmployee(@ModelAttribute("employee") Employee employee)  {
        service.saveEmployee(employee);

        return("redirect:/employees");
    }

    @GetMapping("/update-employee/{id}")
    public String viewUpdateEmployee(@PathVariable (value = "id") Integer id, Model model) {
        Employee employee = service.getEmployeeById(id);

        model.addAttribute("employee", employee);
        return "update_employee_form";
    }

    @GetMapping("/delete-employee/{id}")
    public String deleteEmployee(@PathVariable (value = "id") Integer id) {
        service.deleteEmployee(id);
        return("redirect:/employees");
    }
}
