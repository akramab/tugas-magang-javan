package com.javanlabs.tugas4crud.repository;

import com.javanlabs.tugas4crud.entity.Company;
import com.javanlabs.tugas4crud.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRepository extends JpaRepository<Company, Integer> {
    Company findByNama(String nama);
}
