package com.javanlabs.tugas4crud.repository;

import com.javanlabs.tugas4crud.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    Employee findByNama(String nama);
}
