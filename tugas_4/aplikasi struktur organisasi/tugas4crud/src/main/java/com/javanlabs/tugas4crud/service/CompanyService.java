package com.javanlabs.tugas4crud.service;

import com.javanlabs.tugas4crud.entity.Company;
import com.javanlabs.tugas4crud.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {

    @Autowired
    private CompanyRepository repository;

    public Company saveCompany(Company company) {
        return repository.save(company);
    }
    public List<Company> saveCompanies(List<Company> companies) {
        return repository.saveAll(companies);
    }

    public List<Company> getCompanies() {
        return repository.findAll();
    }
    public Company getCompanyById(int id) {
        return repository.findById(id).orElse(null);
    }
    public Company getCompanyByNama(String nama) {
        return repository.findByNama(nama);
    }

    public String deleteCompany(int id) {
        repository.deleteById(id);
        return "Company removed: " + id;
    }

    public Company updateCompany(Company company) {
        Company existingCompany = repository.findById(company.getId()).orElse(null);

        existingCompany.setNama(company.getNama());
        existingCompany.setAlamat(company.getAlamat());

        return repository.save(existingCompany);
    }
}
