package com.javanlabs.tugas4crud.service;

import com.javanlabs.tugas4crud.entity.Employee;
import com.javanlabs.tugas4crud.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository repository;

    public Employee saveEmployee(Employee employee) {
        return repository.save(employee);
    }
    public List<Employee> saveEmployees(List<Employee> employees) {
        return repository.saveAll(employees);
    }

    public List<Employee> getEmployees() {
        return repository.findAll();
    }
    public Employee getEmployeeById(int id) {
        return repository.findById(id).orElse(null);
    }
    public Employee getEmployeeByNama(String nama) {
        return repository.findByNama(nama);
    }

    public String deleteEmployee(int id) {
        repository.deleteById(id);
        return "Employee removed: " + id;
    }

    public Employee updateEmployee(Employee employee) {
        Employee existingEmployee = repository.findById(employee.getId()).orElse(null);

        existingEmployee.setNama(employee.getNama());
        existingEmployee.setAtasan_id(employee.getAtasan_id());
        existingEmployee.setCompany_id(employee.getCompany_id());

        return repository.save(existingEmployee);
    }
}
