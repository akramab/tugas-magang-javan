package com.javanlabs.tugas5export;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tugas5exportApplication {

	public static void main(String[] args) {
		SpringApplication.run(Tugas5exportApplication.class, args);
	}

}
