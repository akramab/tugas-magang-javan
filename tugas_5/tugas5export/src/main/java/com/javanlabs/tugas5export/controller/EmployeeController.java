package com.javanlabs.tugas5export.controller;

import com.javanlabs.tugas5export.entity.Employee;
import com.javanlabs.tugas5export.entity.ExcelExporter;
import com.javanlabs.tugas5export.entity.PDFExporter;
import com.javanlabs.tugas5export.service.EmployeeService;
import com.lowagie.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class EmployeeController {

    @Autowired
    private EmployeeService service;

    //For web pages
    @GetMapping("/employees")
    public String viewEmployees(Model model) {
        model.addAttribute("listOfEmployees", service.getEmployees());

        return "employees_list";
    }

    @GetMapping("/new-employee-form")
    public String viewNewEmployeeForm(Model model) {

        Employee employee = new Employee();
        model.addAttribute("employee", employee);
        return "new_employee_form";
    }

    @PostMapping("/addEmployee")
    public String addEmployee(@ModelAttribute("employee") Employee employee)  {
        service.saveEmployee(employee);

        return("redirect:/employees");
    }

    @GetMapping("/update-employee/{id}")
    public String viewUpdateEmployee(@PathVariable (value = "id") Integer id, Model model) {
        Employee employee = service.getEmployeeById(id);

        model.addAttribute("employee", employee);
        return "update_employee_form";
    }

    @GetMapping("/delete-employee/{id}")
    public String deleteEmployee(@PathVariable (value = "id") Integer id) {
        service.deleteEmployee(id);
        return("redirect:/employees");
    }

    @GetMapping("/employees/export/pdf")
    public void exportToPDF(HttpServletResponse response) throws DocumentException, IOException {
        response.setContentType("application/pdf");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=employees_" + currentDateTime + ".pdf";
        response.setHeader(headerKey, headerValue);

        List<Employee> listEmployees = service.getEmployees();

        PDFExporter exporter = new PDFExporter(listEmployees);
        exporter.export(response);

    }

    @GetMapping("/employees/export/excel")
    public void exportToExcel(HttpServletResponse response) throws IOException {
        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=employees_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);

        List<Employee> listEmployees = service.getEmployees();

        ExcelExporter excelExporter = new ExcelExporter(listEmployees);

        excelExporter.export(response);
    }
}
