package com.javanlabs.tugas5export.repository;

import com.javanlabs.tugas5export.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRepository extends JpaRepository<Company, Integer> {
    Company findByNama(String nama);
}
