package com.javanlabs.tugas5export.repository;

import com.javanlabs.tugas5export.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    Employee findByNama(String nama);
}
