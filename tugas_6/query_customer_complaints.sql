--Jumlah komplain setiap bulan

SELECT MONTH(`Date Received`) AS Bulan, COUNT(*) AS JumlahKomplain
FROM consumercomplaints
GROUP BY MONTH(`Date Received`)
ORDER BY Bulan ASC;

--Komplain yang memiliki tags ‘Older American’

SELECT *
FROM consumercomplaints
WHERE Tags = 'Older American';

--View

CREATE VIEW `company response data`
AS
(SELECT  Company,
        SUM(CASE WHEN `Company Response to Consumer` = 'Closed' THEN 1 ELSE 0 END) AS Closed,
        SUM(CASE WHEN `Company Response to Consumer` = 'Closed with explanation' THEN 1 ELSE 0 END) AS `Closed with explanation`,
        SUM(CASE WHEN `Company Response to Consumer` = 'Closed with non-monetary relief' THEN 1 ELSE 0 END) AS `Closed with non-monetary relief`
FROM consumercomplaints
GROUP BY Company);

SELECT * FROM `company response data`;