-- NASales percentage
SELECT ROUND(SUM(NA_Sales),5) AS NorthAmericaSales,
       ROUND((SUM(NA_Sales) + SUM(EU_Sales) + SUM(JP_Sales) + SUM(Other_Sales)),5) AS TotalSales,
       CONCAT((ROUND((SUM(NA_Sales)/(SUM(NA_Sales) + SUM(EU_Sales) + SUM(JP_Sales) + SUM(Other_Sales))),5)*100),'%') AS Percentage
FROM consolegames;

-- ConsoleGames OrderBy
SELECT Name AS ConsoleGameTitles
FROM consolegames
ORDER BY Platform ASC, Year DESC;

-- First4Letters
SELECT Name AS GameTitles,
       SUBSTRING(Publisher,1,4) AS First4PublisherLetters
FROM consolegames;

-- BlackFriday /Christmas
SELECT Platform,
       FirstRetailAvailability AS TanggalRilis
FROM consoledates

-- Before Black Friday (black friday jatuh di hari jumat terakhir di bulan november)
-- Setelah melihat dataset, sepertinya tidak ada yang memenuhi
-- Sehingga untuk kasus ini, saya ambil yang jatuhnya tepat di Jumat terakhir setiap bulan november
WHERE FirstRetailAvailability = DATE(DATE_SUB(LAST_DAY(FirstRetailAvailability),
        INTERVAL ((WEEKDAY(LAST_DAY(FirstRetailAvailability))+7-4))%7 DAY))
AND MONTH(FirstRetailAvailability) = 11
-- Before Christmas (asumsi christmas selalu jatuh tanggal 24 desember)
-- Jadi yang diambil adalah yang rilis di tanggal 23
OR FirstRetailAvailability LIKE '%-12-23';

-- Longevity
SELECT Platform,
       (CASE WHEN Discontinued IS NULL THEN DATEDIFF(CURRENT_DATE, FirstRetailAvailability)
                ELSE DATEDIFF(Discontinued, FirstRetailAvailability) END) AS Longevity
FROM consoledates
ORDER BY Longevity ASC;

-- Contoh ini menunjukan pengubahan tipe dari int ke char
SELECT Year AS OriginalType,
       CONVERT(Year, CHAR) AS ConvertedToChar
FROM consolegames;