-- Pets & Owner's info
SELECT *
FROM pets
JOIN owners
ON pets.OwnerID = owners.OwnerID;

-- Pets & Procedure performed
SELECT DISTINCT pets.PetID, Name, Kind
FROM pets
JOIN procedureshistory AS p
ON pets.PetID = p.PetID;

-- Match to Desc
SELECT ph.PetID, ph.Date, ph.ProcedureType, ph.ProcedureSubCode, pd.Description
FROM procedureshistory AS ph
JOIN proceduresdetails AS pd
ON ph.ProcedureType = pd.ProcedureType
AND ph.ProcedureSubCode = pd.ProcedureSubCode;

-- Match to Desc this clinic
SELECT ph.PetID, ph.Date, ph.ProcedureType, ph.ProcedureSubCode, pd.Description
FROM procedureshistory AS ph
JOIN proceduresdetails AS pd
ON ph.ProcedureType = pd.ProcedureType
AND ph.ProcedureSubCode = pd.ProcedureSubCode
JOIN pets
ON pets.PetID = ph.PetID;

-- Pets owner & price
SELECT ph.PetID,owners.OwnerID, owners.Name, owners.Surname, owners.StreetAddress, owners.City, owners.State, owners.StateFull, owners.ZipCode, pd.Price
FROM owners
JOIN pets
ON owners.OwnerID = pets.OwnerID
JOIN procedureshistory AS ph
ON ph.PetID = pets.PetID
JOIN proceduresdetails AS pd
ON pd.ProcedureType = ph.ProcedureType
AND pd.ProcedureSubCode = ph.ProcedureSubCode;