create table customer
(
	CustomerID int not null,
	Name varchar(50) not null,
	surname varchar(50) not null,
	Shipping_State varchar(50) not null,
    Loyalty_Discount int not,
	constraint customer_pk
		primary key (CustomerID)
);

create table item
(
	ItemID int not null,
	Description TEXT not null,
	Retail_Price int not null,
	constraint item_pk
		primary key (ItemID)
);

create table transaction
(
	TransactionID int not null,
	CustomerID int not null,
	ItemID int not null,
	Timestamp DATETIME not null,
	constraint transaction_pk
		primary key (TransactionID)
);